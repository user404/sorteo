<!-- escritura/index  -->
@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_content">
      <div class="x_panel">
              <p style="font-size:40px; text-align: center;"> {{$titulo}} </p> 

         @include('partials.flash-message')
          <div class="x_title">
          </div>
           <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
      </div>
    </div>
  </div>
</div>  

<!-- Modal -->
<div class="modal fade" id="myModalGanador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('images/fondo1.jpg') }}) ; background-position: center; ">
  <div class="modal-dialog modal-lg" role="document"  style="opacity:0.9 !important; top: 24%;">



  <form id ="myFormGanador" method="post" action={{ url('aspirante')}} >
        @csrf
        <input name="_method" type="hidden" value="PATCH">

    <div class="modal-content">

    <div class="modal-header">
        <h2 id="modal_titulo" style="font-size:40px; color: black; text-align: center;"> <b><u>  SORTEO </b></u>  </h2>
      </div>

    <br>
      <div class="modal-body" id = "modal_ganador_texto" style="font-size:40px; color: black;">
      
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnGanador" name="btnGanador" class="btn btn-default"> Aceptar </button>
    
        
      </div>
    </div>
    </form>
  </div>
</div>

@endsection

@section('javascripts')
<script type="text/javascript" src="{{ url('datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('datatables/responsive/js/dataTables.responsive.min.js') }}"></script>

<script>

  $('#mensaje_alerta').toggle();

  

  var table;
  $(document).ready(function() { 

      table = $('#datatable-responsive').DataTable({
      "responsive" :true,
//      "ordering": false,
      "pageLength": 25,
      "ajax": "{{ url('sorteos_ajax') }}",
      "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningun dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Ultimo",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
            "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
// Desarrollado por departamento de informatica.
      "columns": [
                  {"data":"id","visible": false, "title" : "id", "orderable": true},
                  {"data":"nombre","visible": true, "title" : "Sorteo", "orderable": true},
                  {"data":"aspirantes","visible": true, "title" : "Cantidad de aspirantes", "orderable": true},

                  {"data": null, "render": function ( data, type, full, meta ) {
                    if(data.activo == 1){
                      return "Activo";
                    }
                    else {
                      return "Cerrado";
                    }
                      }, "title" : "Estado", "orderable": true},


                  {"data": null, "render": function ( data, type, full, meta ) {
                    if(data.ganador != 1){
                      return "<button id='btnModal' type='buttom' data-toggle='modal' data-estado='" + data.activo + "' data-sorteo =  '" + data.nombre + "' data-id=' " + data.id + " ' data-target='#myModalGanador' class='btn btn-dark btn-flat' width='30px' ><i class='glyphicon glyphicon-minus'></i></a>";
                    }
                    else {
                      return null;
                    }
                      }, "title" : "Cerrar", "orderable": true},
                ],
      "createdRow": function( row, data, dataIndex){
            },
      "lengthMenu": [[ 10, 25, 50, -1], [ 10, 25, 50, "TODOS"]]
  
    });

  $('#datatable-responsive_filter').hide();
  
  $('#datatable-responsive select option:contains("it\'s me")').prop('selected',true); 
      
  $('#datatable-responsive thead th').each( function (row, i, start, end, display ) {
    if(row != 1 && row != 2 && row != 3){
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar por '+title+'" />' );
    }
    });


    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
     });


  });

var id;
  


$('#myModalGanador').on('show.bs.modal', function(e) {
    var $modal = $(this);
    var button = $(e.relatedTarget);
    id = button.data('id');
    
    var sorteo = button.data('sorteo');
    var estado = button.data('estado');

    $('#myFormGanador').attr("action", 'aspirante/' + id.replace(/\s/g, ''));
    $("#id_borrar").val(id);
    $("#modal_ganador_texto").html('Seguro desea cambiar el estado del sorteo <b>' + sorteo +  ' </b>?');
})

$( "#btnGanador").click(function() {
  
  var str = "{{ URL::to('sorteo', 'ID') }}";
  var res = str.replace("ID", id);

   $.ajax({
          type: "POST",                 
          url: res,                     
          data: $("#myFormGanador").serialize(), 
          success: function(result)             
          {
              $('#mensaje_alerta').toggle(1500);
              $('#myModalGanador').modal('toggle'); 
              table.ajax.reload( null, false );    
          }
       });

});



$( "#btnActualizar" ).click(function() {

 


});

</script>
@endsection