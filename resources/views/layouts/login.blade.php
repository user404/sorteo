<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
    <title>Sistema de Sorteos IPDUV </title>

    <!-- Bootstrap -->
    <!--link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"-->
    <link href="{{ url('js/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">


    <!-- Custom Theme Style -->
    <!--link href="../build/css/custom.min.css" rel="stylesheet"-->
    <link href="{{ url('assets/build/css/custom.min.css') }}" rel="stylesheet">
    
    <style type="text/css">
    .fondo{
    background-repeat: no-repeat;
    background-image: url('./images/background_1.jpg');
    background-size: cover;
    }

    </style>
  </head>

  <body class="fondo">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            
              <h1><img src="{{ url('assets/images/logo.png') }}"> </h1>
                @yield('content')
                @include('partials.flash-message')
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Nuevo usuario?
                   <a href="#signup" class="to_register"> Crear Usuario </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1> DEPARTAMENTO DE SISTEMAS</h1>
                  <p>©2017 Prog. Barraza, Federico</p>
                </div>
              </div>
            
          </section>
        </div>        
      </div>
    </div>
    
  </body>
</html>