@extends('layouts.login')

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div>
                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder='Usuario/E-mail' required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

            <div>
                <input id="password" type="password" class="form-control" name="password" placeholder="Clave" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

                <div class="col-md-7">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </div>
                </div>
                <div class="col-md-5">
                <button type="submit" class="btn btn-default">
                    Ingresar
                </button>
                </div>
        </div>
    </form>

@endsection
