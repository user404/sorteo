@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">

  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
    
      <div class="panel-heading">Cargar excel</div>
        <div class="panel-body">

        <div id="mensaje_recordatorio" class="alert alert-warning alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <p> Para que el sistema funciona correctamente se recomienda que el archivo a cargar sea .XLSX y los campos deben estar en el siguiente orden 
          <br> <b>CANT</b>
          <br> <b>Nº Inscripción</b>
          <br> <b>Titular</b> 
          <br> <b>Nº Documento</b>
          </p>
        </div> 

          @include('partials.flash-message')

          <form method="POST" id="frmPrincipal" action={{ url('import')}} accept-charset="UTF-8" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
              <label class="col-md-3 control-label">Sorteo</label>
              <div class="col-md-4 col-sm-2 col-xs-12">
                <select class="form-control" name="sorteo_id" id="sorteo_id">
                  <option value="" disabled selected>Seleccione o cargue nombre</option>
                  @foreach($sorteos as $s)
                    <option value="{{$s->id}}">{{$s->nombre}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-md-1 col-sm-1 col-xs-1">
                <button type="button" id="btnModal" data-toggle="modal" data-target="#myModal" class="btn btn-default">
                  <i class="glyphicon glyphicon-plus"></i>
                </button>
              </div>
            </div>

            <br><br>

            <div class="form-group">
              <label class="col-md-3 control-label">Categoria</label>
              <div class="col-md-4 col-sm-2 col-xs-12">
              <select class="form-control" id="categoria_id" name="categoria_id">
                <option value="" disabled selected>Seleccione una categoria</option>
                @foreach($categorias as $c)
                  <option value="{{$c->id}}">{{$c->nombre}}</option>
                @endforeach
              </select>
              </div>

              <div class="col-md-1 col-sm-1 col-xs-1">
                <button type="button" id="btnModalCategoria" data-toggle="modal" data-target="#myModalCategoria" class="btn btn-default">
                  <i class="glyphicon glyphicon-plus"></i>
                </button>
              </div>
            </div>
            
            <br><br>
            
            <div class="form-group">             
              <label class="col-md-3 control-label">Seleccione el archivo</label>
              <div class="col-md-6">
                <input type="file" class="form-control" name="file">
              </div>
            </div>
            
            <br><br>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" id="submitBtn" class="btn btn-primary">Cargar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">


  <form id = "formSorteo" method="POST" action={{ url('sorteo')}} accept-charset="UTF-8" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="modal-content">
      
      <div class="modal-header">
        <h2 id="modal_titulo">Nuevo sorteo</h2>
      </div>
      <div class="modal-body">

        <div id="mensaje_alerta" class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <p> El sorteo se agrego correctamente </p>
        </div>  

           <div class="form-group">
            <div class="col-md-3 control"> 
              <h5> Nombre:</h5>
            </div> 
            <div class="col-md-6 col-sm-4 col-xs-12">
            <input type="text" class="form-control"  id ="nombre" name="nombre">
            </div>
          </div>
      </div>
      <br><br>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id ="btnSorteo" type="button" class="btn btn-primary">Agregar</button>
      </div>
    </form>
  </div>
</div>
</div>

<!-- Modal Categoria-->
<div class="modal fade" id="myModalCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">


  <form id = "formCategoria" method="POST" action={{ url('categoria')}} accept-charset="UTF-8" enctype="multipart/form-data">            
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="modal-content">
      
      <div class="modal-header">
        <h2 id="modal_titulo">Nueva categoria</h2>
      </div>
      <div class="modal-body">

        <div id="mensajeAlertaCategoria" class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <p> La categoria se agrego correctamente </p>
        </div>  

           <div class="form-group">
            <div class="col-md-3 control"> 
              <h5> Nombre:</h5>
            </div> 
            <div class="col-md-6 col-sm-4 col-xs-12">
            <input type="text" class="form-control"  id ="nombreCategoria" name="nombreCategoria">
            </div>
          </div>
      </div>
      <br><br>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id ="btnCategoria" type="button" class="btn btn-primary">Agregar</button>
      </div>
    </form>
  </div>
</div>
</div>

@endsection

@section('javascripts')

<script> 

$("#frmPrincipal").submit(function () {
        $("#submitBtn").attr("disabled", true);
        return true;
  });



$('#mensaje_alerta').toggle();

  var res = "{{ URL::to('sorteo') }}";


$( "#btnSorteo" ).click(function() {
  var ultimo;   
  $.ajax({                        
    type: "post",                 
    url: res,                     
    data: $("#formSorteo").serialize(), 
    success: function(result)             
    {
//      $('#mensaje_recordatorio').hide();
      $('#mensaje_alerta').toggle(3000);

      setTimeout(function ()
      {
        $('#myModal').modal('toggle');         
      }, 3700);

      ultimo = $("#nombre").val();
      var options = [];
      for (var i = 0; i < Object.keys(result).length; i++) {
        options.push('<option value="',
        result[i].id, '">',
        result[i].nombre, '</option>');
      }

      $("#sorteo_id").html(options.join(''));  
      $('#sorteo_id option:contains(' + ultimo + ')').prop('selected',true);        
    }
 });


});

$('#myModal').on('show.bs.modal', function(e) { 
//   $("#pago").text('');
  $('#mensaje_alerta').hide();
  $('#nombre').val('');
})

$('#myModalCategoria').on('show.bs.modal', function(e) { 
  $('#mensajeAlertaCategoria').hide();
  $('#nombreCategoria').val('');
})

$( "#btnCategoria" ).click(function() {
var url = "{{ URL::to('categoria') }}";
var ultimo;
 
$.ajax({                        
  type: "post",                 
  url: url,                     
  data: $("#formCategoria").serialize(), 
  success: function(result)             
  {
    console.log(result);
//    $('#mensaje_recordatorio').hide();
    $('#mensajeAlertaCategoria').toggle(3000);

    setTimeout(function ()
    {
      $('#myModalCategoria').modal('toggle');         
    }, 3700);

    ultimo = $("#nombreCategoria").val();
    var options = [];
    for (var i = 0; i < Object.keys(result).length; i++) {
      console.log(result[i].nombre);
      options.push('<option value="',
      result[i].id, '">',
      result[i].nombre, '</option>');
    }

    $("#categoria_id").html(options.join(''));  
    $('#categoria_id option:contains(' + ultimo + ')').prop('selected',true);        
  }
});


});

/*
  $(document).on("click", "#btnPDF", function () {
    var w = window.open(this.value, 'popUpWindow','height=600,width=800,left=10,top=10,,scrollbars=yes,menubar=no'); return false;
  });
*/
</script>

@endsection