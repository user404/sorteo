<!-- escritura/index  -->
@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">

<style>
.bolded {
  font-weight:bold;
  font-size: 15px;
}
</style>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_content">
      <div class="x_panel">         
        <div class="col-md-6">
          <p style="font-size:20px; text-align: center;"> Ganadores de sorteos anteriores </p> 
          <table id="datatable-ganadores" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
        </div>

        <div class="col-md-6">
          <p style="font-size:20px; text-align: center;" id='noParticipanTitulo'></p> 
          <p style="font-size:15px; text-align: center;" id='noParticipanCuerpo'></p> 
          <table id="datatable-excluidos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
        </div>

      </div>
    </div>
    

       
    <div class="x_content">
      <div class="x_panel">
        <p style="font-size:20px; text-align: center;"> Lista de aspirantes </p> 
        <div class="x_title">
        </div>          
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
      </div>
    </div>
  </div>
</div>  

<!-- Modal -->
<!--
  <div class="modal fade" id="myModalGanador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('images/fondo2.jpg') }}) ; background-position: center; ">
-->
<div class="modal fade" id="myModalGanador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-image: url({{ url('images/fondo.jpg') }}) ; background-position: center; ">
  <div class="modal-dialog modal-lg" role="document"  style="opacity:0.9 !important; top: 15%;">



  <form id ="myFormGanador" method="post" action={{ url('aspirante')}} >
        @csrf
        <input name="_method" type="hidden" value="PATCH">

    <div class="modal-content">

    <div class="modal-header">
        <h2 id="modal_titulo" style="font-size:50px; color: black; text-align: center;"> <b><u>  SORTEADO </b></u>  </h2>
      </div>

    <br>
      <div class="modal-body" id = "modal_ganador_texto" style="font-size:50px; color: black;">
      
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnGanador" name="btnGanador" class="btn btn-default"> Aceptar </button>
    
        
      </div>
    </div>
    </form>
  </div>
</div>

@endsection

@section('javascripts')
<script type="text/javascript" src="{{ url('datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('datatables/responsive/js/dataTables.responsive.min.js') }}"></script>

<script>

  $('#mensaje_alerta').toggle();

  

  var table;
  var tableGanadores;
  var tableExcluidos;
  $(document).ready(function() { 

      table = $('#datatable-responsive').DataTable({
 
        "initComplete": function(row, i, start, end, display) {


$("#datatable-responsive thead th").each( function (row, i, start, end, display ) {
//		console.log(row);

    if(row == 2 || row == 3)
{
		if ($(this).text() !== '') {
      var isStatusColumn = (($(this).text() == 'Status') ? true : false);
			var select = $('<select class="form-control" id= "cabecera' + row + '" style="height:28px; font-size:13px;"><option value=""></option></select>')
          .appendTo( $(this).empty() )
          .on( 'change', function () {

            $('#noParticipanTitulo').html('Siguiente sorteo <b>' + $("#cabecera3").val() + '</b> no participan');

            tableExcluidos.ajax.reload( null, false );  
            var val = $(this).val();		
            table.column( i )
                .search( val ? '^'+$(this).val()+'$' : val, true, false )
                .draw();
          } );
	 		
			// Get the Status values a specific way since the status is a anchor/image

			if (isStatusColumn) {
				var statusItems = [];				
       /* ### IS THERE A BETTER/SIMPLER WAY TO GET A UNIQUE ARRAY OF <TD> data-filter ATTRIBUTES? ### */
				table.column( i ).nodes().to$().each( function(d, j){
					var thisStatus = $(j).attr("data-filter");
					if($.inArray(thisStatus, statusItems) === -1) statusItems.push(thisStatus);
				} );
				
				statusItems.sort();
								
				$.each( statusItems, function(i, item){
				    select.append( '<option value="'+item+'">'+item+'</option>' );
				});

			}
            // All other non-Status columns (like the example)
			else {
				table.column( i ).data().unique().sort().each( function ( d, j ) {  
					select.append( '<option value="'+d+'">'+d+'</option>' );
		        } );	
			}
		}
}

    } );

        },
   
      "responsive" :true,
      "ordering": false,
      "pageLength": 50,
      "ajax": "{{ url('aspirantes_ajax') }}",
      "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningun dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Ultimo",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
            },
            "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
// Desarrollado por departamento de informatica.
      "columns": [
                  {"data":"num_orden", "visible": true, "title" : "Orden", "orderable": false},
                  {"data":"num_inscripcion","visible": false, "title" : "Nº de inscripcion", "orderable": false},
                  {"data":"titular","visible": true, "title" : "Titular", "orderable": false},
                  {"data":"num_doc","visible": false, "title" : "Documento", "orderable": false},
                  {"data":"sorteo","visible": true, "title" : "Sorteo", "orderable": false},
                  {"data":"categoria","visible": true, "title" : "Categoria", "orderable": false},   

                  {"data": null, "render": function ( data, type, full, meta ) {
                    if(data.ganador != 1){
                      return "<button id='btnModal' type='buttom' data-toggle='modal' data-categoria =  '" + data.categoria + "' data-id=' " + data.id + " ' data-titular=' " + data.titular + " ' data-documento = '" + data.num_doc + "' data-orden = '" + data.num_orden + "' data-target='#myModalGanador' class='btn btn-dark btn-flat' width='30px' ><i class='glyphicon glyphicon-star'></i></a>";
                    }
                    else {
                      return null;
                    }
                      }, "title" : "Ganador", "orderable": false},
                ],
      "createdRow": function( row, data, dataIndex){
//        alert(data.categoria);

        if(data.ganador == 1){
            $(row).css({"background-color":"#ffc000"})
          }
            },
      "lengthMenu": [[ 10, 25, 50, -1], [ 10, 25, 50, "TODOS"]]
  
//      ,
//      "sDom": '<"top"i>rt<"bottom"flp><"clear">'

  
    });

  $('#datatable-responsive_filter').hide();
  
  $('#datatable-responsive select option:contains("it\'s me")').prop('selected',true); 
      
  $('#datatable-responsive thead th').each( function (row, i, start, end, display ) {
    if(row != 2 && row != 3 && row != 4){
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar por '+title+'" />' );
    }
    });


    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
     });


// INICIO TABLE GANADORES
tableGanadores = $('#datatable-ganadores').DataTable({
 
 "initComplete": function(row, i, start, end, display) {


$("#datatable-ganadores thead th").each( function (row, i, start, end, display ) {
//		console.log(row);

if(row == 2 || row == 3)
{
if ($(this).text() !== '') {
var isStatusColumn = (($(this).text() == 'Status') ? true : false);
var select = $('<select class="form-control" id= "otro' + row + '" style="height:28px; font-size:13px;"><option value=""></option></select>')
   .appendTo( $(this).empty() )
   .on( 'change', function () {

     var val = $(this).val();		
     tableGanadores.column( i )
         .search( val ? '^'+$(this).val()+'$' : val, true, false )
         .draw();
   } );

// Get the Status values a specific way since the status is a anchor/image

if (isStatusColumn) {
 var statusItems = [];				
/* ### IS THERE A BETTER/SIMPLER WAY TO GET A UNIQUE ARRAY OF <TD> data-filter ATTRIBUTES? ### */
  tableGanadores.column( i ).nodes().to$().each( function(d, j){
   var thisStatus = $(j).attr("data-filter");
   if($.inArray(thisStatus, statusItems) === -1) statusItems.push(thisStatus);
 } );
 
 statusItems.sort();
         
 $.each( statusItems, function(i, item){
     select.append( '<option value="'+item+'">'+item+'</option>' );
 });

}
     // All other non-Status columns (like the example)
else {
  tableGanadores.column( i ).data().unique().sort().each( function ( d, j ) {  
   select.append( '<option value="'+d+'">'+d+'</option>' );
     } );	
}
}
}

} );

 },

"responsive" :true,
"ordering": false,
"pageLength": 50,
"ajax": "{{ url('ganadores_ajax') }}",
"language": {
     "sProcessing":     "Procesando...",
     "sLengthMenu":     "Mostrar _MENU_ registros",
     "sZeroRecords":    "No se encontraron resultados",
     "sEmptyTable":     "Ningun dato disponible en esta tabla",
     "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
     "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
     "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
     "sInfoPostFix":    "",
     "sSearch":         "Buscar:",
     "sUrl":            "",
     "sInfoThousands":  ",",
     "sLoadingRecords": "Cargando...",
     "oPaginate": {
       "sFirst":    "Primero",
       "sLast":     "Ultimo",
       "sNext":     "Siguiente",
       "sPrevious": "Anterior"
     },
     "oAria": {
       "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
       "sSortDescending": ": Activar para ordenar la columna de manera descendente"
     }
 },
// Desarrollado por departamento de informatica.
"columns": [
           {"data":"num_orden","visible": true, "title" : "Orden", "orderable": false},
           {"data":"num_inscripcion","visible": false, "title" : "Nº de inscripcion", "orderable": false},
           {"data":"titular","visible": true, "title" : "Titular", "orderable": false},
           {"data":"num_doc","visible": false, "title" : "Documento", "orderable": false},
           {"data":"categoria","visible": true, "title" : "Categoria", "orderable": false},   
         ],
"createdRow": function( row, data, dataIndex){
//        alert(data.categoria);

 if(data.ganador == 1){
     $(row).css({"background-color":"#ffc000"})
   }
     },
"lengthMenu": [[ 10, 25, 50, -1], [ 10, 25, 50, "TODOS"]]

//      ,
//      "sDom": '<"top"i>rt<"bottom"flp><"clear">'


});

$('#datatable-ganadores_filter').hide();

$('#datatable-ganadores select option:contains("it\'s me")').prop('selected',true); 

$('#datatable-ganadores thead th').each( function (row, i, start, end, display ) {
if(row != 2 && row != 3 && row != 4){
 var title = $(this).text();
 $(this).html( '<input type="text" placeholder="Buscar por '+title+'" />' );
}
});


tableGanadores.columns().every( function () {
 var that = this;

 $( 'input', this.header() ).on( 'keyup change', function () {
     if ( that.search() !== this.value ) {
         that
             .search( this.value )
             .draw();
     }
 } );
});
// FIN TABLE GANADORES

// INICIO TABLE EXCLUIDOS
tableExcluidos = $('#datatable-excluidos').DataTable({
 
 "initComplete": function(row, i, start, end, display) {


$("#datatable-excluidos thead th").each( function (row, i, start, end, display ) {} );
},

"responsive" :true,
"ordering": false,
"pageLength": 50,
"ajax": {
    "url": "{{ url('excluidos_ajax') }}",
    "data": function ( d ) {
      return $.extend( {}, d, {
        "sorteo": $("#cabecera2").val(),
        "categoria": $("#cabecera3").val()
      } );
    }
  },
"language": {
     "sProcessing":     "Procesando...",
     "sLengthMenu":     "Mostrar _MENU_ registros",
     "sZeroRecords":    "No se encontraron resultados",
     "sEmptyTable":     "Ningun dato disponible en esta tabla",
     "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
     "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
     "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
     "sInfoPostFix":    "",
     "sSearch":         "Buscar:",
     "sUrl":            "",
     "sInfoThousands":  ",",
     "sLoadingRecords": "Cargando...",
     "oPaginate": {
       "sFirst":    "Primero",
       "sLast":     "Ultimo",
       "sNext":     "Siguiente",
       "sPrevious": "Anterior"
     },
     "oAria": {
       "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
       "sSortDescending": ": Activar para ordenar la columna de manera descendente"
     }
 },
// Desarrollado por departamento de informatica.
"columns": [
           {"className":"bolded", "data":"num_orden","visible": true, "title" : "Orden", "orderable": false},
           {"data":"titular","visible": true, "title" : "Titular", "orderable": false},
         ],
"createdRow": function( row, data, dataIndex){
     },
"lengthMenu": [[ 10, 25, 50, -1], [ 10, 25, 50, "TODOS"]]

});

$('#datatable-excluidos_filter').hide();

// FIN TABLE EXCLUIDOS



  });

var id;
  


$('#myModalGanador').on('show.bs.modal', function(e) {
    var $modal = $(this);
    var button = $(e.relatedTarget);
    id = button.data('id');
    var titular = button.data('titular');
    var documento = button.data('documento');

    var orden = button.data('orden');
    var categoria = button.data('categoria');

//    alert(categoria);

    $('#myFormGanador').attr("action", 'aspirante/' + id.replace(/\s/g, ''));
    $("#id_borrar").val(id);
    $("#modal_ganador_texto").html('<b> Orden: </b>' + orden + '<br> <b> Nombre : </b>' + titular + ' <br> <b> Documento : </b>' + documento + '<br> <b> Padron de sorteo: </b>' + categoria );
})

$( "#btnGanador").click(function() {
  
  var str = "{{ URL::to('aspirante', 'ID') }}";
  var res = str.replace("ID", id);

   $.ajax({                        
          type: "POST",                 
          url: res,                     
          data: $("#myFormGanador").serialize(), 
          success: function(result)             
          {
            if(result == 'existente')
            {
              alert('LA PERSONA SELECCIONADA YA GANO OTRO SORTEO')
            } else {
              $('#myModalGanador').modal('toggle'); 
              table.ajax.reload( null, false );    
              tableGanadores.ajax.reload( null, false );
              tableExcluidos.ajax.reload( null, false );
            }

//            $('#mensaje_alerta').toggle(1500);

  

 

//            $('#sorteo_id option:contains(' + ultimo + ')').prop('selected',true);        
          }
       });

});


//ganadores


$( "#btnActualizar" ).click(function() {});

</script>
@endsection