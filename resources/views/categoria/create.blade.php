@extends('layouts.principal')
@section('styles')
<link href="{{ url('datatables/bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('datatables/scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">

  <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
    
      <div class="panel-heading">Cargar nueva categoria</div>
        <div class="panel-body">

        <div id="mensaje_recordatorio" class="alert alert-warning alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <p> Cargue las categorias correspondientes para los sorteos 
          </p>
        </div> 

          @include('partials.flash-message')

          <form method="POST" id="frmPrincipal" action={{ url('import')}} accept-charset="UTF-8" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <form method="post" action="{{url('passports')}}" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                  <label for="nombre">Nombre categoria:</label>
                  <input type="text" class="form-control" name="nombre">
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                  <button type="submit" id="submitBtn" class="btn btn-primary">Cargar</button>
                </div>
              </div>
           
          </form>
        </div>
      </div>
    </div>
  </div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">


  <form id = "formSorteo" method="POST" action={{ url('sorteo')}} accept-charset="UTF-8" enctype="multipart/form-data">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="modal-content">
      
      <div class="modal-header">
        <h2 id="modal_titulo">Nuevo sorteo</h2>
      </div>
      <div class="modal-body">

        <div id="mensaje_alerta" class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <p> El sorteo se agrego correctamente </p>
        </div>  

           <div class="form-group">
            <div class="col-md-3 control"> 
              <h5> Nombre:</h5>
            </div> 
            <div class="col-md-6 col-sm-4 col-xs-12">
            <input type="text" class="form-control"  id ="nombre" name="nombre">
            </div>
          </div>
      </div>
      <br><br>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id ="btnSorteo" type="button" class="btn btn-primary">Agregar</button>
      </div>
</div>

@endsection

@section('javascripts')

<script> 

$("#frmPrincipal").submit(function () {
        $("#submitBtn").attr("disabled", true);
        return true;
  });



$('#mensaje_alerta').toggle();

  var res = "{{ URL::to('sorteo') }}";


$( "#btnSorteo" ).click(function() {

  var ultimo;
   
  $.ajax({                        
    type: "post",                 
    url: res,                     
    data: $("#formSorteo").serialize(), 
    success: function(result)             
    {
      $('#mensaje_recordatorio').hide();
      $('#mensaje_alerta').toggle(3000);

      setTimeout(function ()
      {
        $('#myModal').modal('toggle');         
      }, 3700);

      ultimo = $("#nombre").val();
      var options = [];
      for (var i = 0; i < Object.keys(result).length; i++) {
        options.push('<option value="',
        result[i].id, '">',
        result[i].nombre, '</option>');
      }

      $("#sorteo_id").html(options.join(''));  
      $('#sorteo_id option:contains(' + ultimo + ')').prop('selected',true);        
    }
 });


});

$('#myModal').on('show.bs.modal', function(e) { 
//   $("#pago").text('');
  $('#mensaje_alerta').hide();
  $('#nombre').val('');
})
/*
  $(document).on("click", "#btnPDF", function () {
    var w = window.open(this.value, 'popUpWindow','height=600,width=800,left=10,top=10,,scrollbars=yes,menubar=no'); return false;
  });
*/
</script>

@endsection