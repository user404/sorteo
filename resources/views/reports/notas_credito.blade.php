
<html>
<head>
  <style>
    body{
      font-family: sans-serif;
      font-size: 9px;
      margin: 0;
      padding: 0;
    }
   
   html {
    margin:0;
    padding:0;  
} 
    table {
      border-collapse: collapse;
    }

    table, th, td {
      border: 1px solid black;
    }

    #content {
      margin:0;
      padding:0;  
    }

    div.titulo {
      text-align: center;
    }
  </style>

<body>
  <header>
    <div class="titulo"> 
      <h1 class="titulo">Reporte para {{$data1[0]->titulo}}</h1>
    </div>
  </header>

  <div id="content">
    <table>
  <thead>
    <tr>
      <th>Jurisdiccion</th>
      <th>Nro_ent</th>
      <th>Fec_generado</th>
      <th>Nro_exp</th>
      <th>Fec_imputada</th>
      <th>Beneficiario</th>
      <th>Monto</th>
      <th>imputacion</th>
      <th>Nota_cred</th>
  
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $a)
      <tr>        
        <td> {{ $a->jurisdiccion }} </td>
        <td> {{ $a->nro_ent }} </td>
        <td> {{ $a->fec_generado }} </td>
        <td> {{ $a->nro_expediente }} </td>
        <td> {{ $a->fec_imputada }} </td>
        <td> {{ $a->beneficiario }} </td>
        <td> {{ $a->monto }} </td>
        <td> {{ $a->imputacion }} </td>
        <td> {{ $a->nota_credito }} </td>
      </tr>
    @endforeach
  </tbody>
</table>

      
  </div>

</body>
</html>