
<html>
<head>
  <style>
    body{
      font-family: sans-serif;
      font-size: 9px;
      margin: 0;
      padding: 0;
    }
   
   html {
    margin:0;
    padding:0;  
} 
    table {
      border-collapse: collapse;
    }

    table, th, td {
      border: 1px solid black;
    }

    #content {
      margin:0;
      padding:0;  
    }

    div.titulo {
      text-align: center;
    }
  </style>

<body>
  <header>
    <div class="titulo"> 
      <h1 class="titulo">Reporte para {{$data1[0]->anio}}</h1>
    </div>
  </header>

  <div id="content">

    <table>
  <thead>
    <tr>
      <th>Jurisdiccion</th>
      <th>Ene</th>
      <th>Feb</th>
      <th>Mar</th>
      <th>Abr</th>
      <th>May</th>
      <th>Jun</th>
      <th>Jul</th>
      <th>Ago</th>
      <th>Sep</th>
      <th>Oct</th>
      <th>Nov</th>
      <th>Dic</th>
      <th>Total</th>      
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $a)
      <tr>        
        <td @if($a->ES_TOTAL == 1) bgcolor="#D3D3D3" @endif> @if($a->ES_TOTAL == 0) {{ $a->jurisdiccion }} @endif </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m1 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m1 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m1 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m1 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m2 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m2 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m2 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m2 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m3 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m3 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m3 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m3 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m4 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m4 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m4 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m4 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m5 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m5 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m5 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m5 }} </td> 

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m6 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m6 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m6 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m6 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m7 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m7 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m7 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m7 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m8 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m8 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m8 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m8 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m9 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m9 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m9 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m9 }} </td> 
          
<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m10 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m10 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m10 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m10 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m11 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m11 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m11 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m11 }} </td>

<td @if( ($a->ES_TOTAL == 1)  && ($a->color_m12 == 0) ) 
          bgcolor="#D3D3D3" 
          @elseif ($a->color_m12 == 1)
          bgcolor="#F1948A"
          @elseif ($a->color_m12 == 2)
          bgcolor="#F7DC6F"
          @endif> {{ $a->m12 }} </td>


<td {{ $a->total }}  </td>


      </tr>
    @endforeach
  </tbody>
</table>

      
  </div>

</body>
</html>