<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_normal = Role::where('name', 'normal')->first();
        $role_admin = Role::where('name', 'admin')->first();

        $user = new User();
        $user->name = 'sistemas';
        $user->email = 'ipduv.sistemas@gmail.com';
        $user->password = bcrypt('informatica');
        if(User::where('email', $user->email)->count() == 0 ){
            $user->save();
            $user->roles()->attach($role_normal);
        }
        
        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('admin');
        if(User::where('email', $user->email)->count() == 0 ){
            $user->save();
            $user->roles()->attach($role_normal);
        }


        $user = new User();
        $user->name = 'samuelo';
        $user->email = 'samuelo@samu.com';
        $user->password = bcrypt('123456');
        if(User::where('email', $user->email)->count() == 0 ){
            $user->save();
            $user->roles()->attach($role_admin);
        }


    }
}
