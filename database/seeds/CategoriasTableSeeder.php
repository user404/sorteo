<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entidad = new Categoria();
		$entidad->nombre = '1 o 2 hijos';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = '3 o más hijos';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'Antiguedad';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'Discapacidad';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'General';
        $entidad->save();

        $entidad = new Categoria();
		$entidad->nombre = 'Sin hijos';
        $entidad->save();

        
        /*
        $entidad = new Categoria();
        $entidad->id = 1;
		$entidad->nombre = 'General';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 2;
		$entidad->nombre = 'Con hijos';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 3;
		$entidad->nombre = 'Sin hijos';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 4;
		$entidad->nombre = 'Con hijos con ingresos';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 5;
		$entidad->nombre = 'Con hijos sin ingresos';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 6;
		$entidad->nombre = 'Sin hijos sin ingreso';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 7;
		$entidad->nombre = 'Sin hijos con ingreso';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 8;
		$entidad->nombre = 'Discapacitados';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 9;
		$entidad->nombre = 'Discapacitados con ingreso';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }

        $entidad = new Categoria();
        $entidad->id = 10;
		$entidad->nombre = 'Discapacitados sin ingreso';
        if(Categoria::where('nombre', $entidad->nombre)->count() == 0 ){
        	$entidad->save();
        }
*/
    }
}
