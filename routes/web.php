<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () { return view('auth/login');});
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');



Route::post('import', 'ImportController@import', ['middleware' => ['auth']]);


Route::resource('aspirante', 'AspiranteController', ['middleware' => ['auth']]);
Route::get('aspirantes_ajax', 'AspiranteController@aspirantes_ajax', ['middleware' => ['auth']]);

Route::get('categorias', 'CategoriaController@listar', ['middleware' => ['auth']]);

Route::resource('sorteo', 'SorteoController', ['middleware' => ['auth']]);
Route::get('sorteos_ajax', 'SorteoController@sorteos_ajax', ['middleware' => ['auth']]);

Route::resource('categoria', 'CategoriaController', ['middleware' => ['auth']]);
Route::get('categorias_ajax', 'CategoriaController@categorias_ajax', ['middleware' => ['auth']]);
// Route::post('sorteo', 'SorteoController@store');

Route::get('ganadores_ajax', 'AspiranteController@ganadores_ajax', ['middleware' => ['auth']]);
Route::get('excluidos_ajax/', 'AspiranteController@excluidos_ajax', ['middleware' => ['auth']]);