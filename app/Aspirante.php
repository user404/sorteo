<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aspirante extends Model
{
    protected $fillable = ['num_orden', 'num_inscripcion', 'titular', 'num_doc',
    'categoria_id', 'sorteo_id', 'ganador', 'created_at', 'updated_at'];
}
