<?php

namespace App\Imports;

use App\Aspirante;
use Maatwebsite\Excel\Concerns\ToModel;


class UsersImport implements ToModel
{

    public $_categoria_id = null;
    public $_sorteo_id = null;

    private $rows = 0;

    public function __construct($categoria_id, $sorteo_id) {
        $this->_categoria_id = $categoria_id;
        $this->_sorteo_id = $sorteo_id;
        }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        ++$this->rows;

        if($this->rows > 1) {
        $num_orden = str_pad($row[0], 4, '0', STR_PAD_LEFT);

        return new Aspirante([
            'num_orden'     => $num_orden,
            'num_inscripcion'    => $row[1], 
            'titular'    => $row[2],
            'num_doc'    => $row[3], 
            'categoria_id'    => $this->_categoria_id, 
            'sorteo_id'    => $this->_sorteo_id, 
            'ganador'    => '0',
         ]);
         $this->data = $data;
        
        }

         
    }

    public function getRowCount(): int
    {
        return $this->rows -1;
    }
}
