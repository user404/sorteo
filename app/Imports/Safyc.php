<?php

namespace App\Imports;

use Illuminate\Database\Eloquent\Model;

class Safyc extends Model
{
    protected $table = 'safyc';
    protected $fillable = ['jurisdiccion_id','nro_ent', 'pago_id', 'fec_generado', 'entregado', 'fec_entregado', 'anulado', 'revertido', 'nro_expediente', 'deuda_id', 'beneficiario', 'beneficiario_alt', 'monto'];

     public function getFile()
    {
        return storage_path('exports') . '/file.csv';
    }

    public function getFilters()
    {
        return [
            'chunk'
        ];
    }

    public function safyc()
    {
        return $this->belongsTo('App\Deuda');
    }

}
