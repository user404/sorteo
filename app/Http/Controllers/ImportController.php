<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\Safyc;
use App\Http\Controllers\Controller;
use Exception;
use DB;
use Storage;
use File;
use Illuminate\Support\Facades\Input;
use App\Aspirante;
use Illuminate\Database\Eloquent\Collection;



use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;


use App\Errores;

class ImportController extends Controller
{
    private $path = 'aspirante';
        
    public function import(Request $request)
    {
        $categoria_id = $request->categoria_id;
        $sorteo_id = $request->sorteo_id;

        $import = new UsersImport($categoria_id, $sorteo_id);
        Excel::import($import, Input::file('file'));

        $categoria = \App\Categoria::find($categoria_id); 
        $sorteo = \App\Sorteo::find($sorteo_id); 
        
        \Session::flash('success', '<b>Se cargaron <u> exitosamente: ' . $import->getRowCount() . ' aspirantes</u> para el sorteo ' . $sorteo->nombre . ' en la categoria ' . $categoria->nombre );

        return back();
    }

	
    }

