<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Collection;
use App\Errores;
use Carbon\Carbon;

class AspiranteController extends Controller
{
    
    private $path = 'aspirante';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo = 'Lista de aspirantes por sorteos';
        return view($this->path.'.index', compact('titulo'));
        return view($this->path. '.index');
    }

    public function aspirantes_ajax (){

        $lista = DB::table('aspirantes')->distinct()
        ->join('sorteos',  'aspirantes.sorteo_id', '=', 'sorteos.id')
        ->join('categorias', 'categorias.id', 'aspirantes.categoria_id')
        ->select(DB::raw('num_orden, num_inscripcion, titular, num_doc, sorteos.nombre as sorteo, 
        ganador, aspirantes.id as id, categorias.nombre as categoria'))
        ->where('activo', '1')
        ->where('ganador', '0')
        ->orderBy('ganador' , 'desc' , 'num_orden', 'asc', 'created_at', 'desc')
        ->simplePaginate(3000);

        return $lista;



        $lista = DB::table('aspirantes')->distinct()
        ->join('sorteos',  'aspirantes.sorteo_id', '=', 'sorteos.id')
        ->join('categorias', 'categorias.id', 'aspirantes.categoria_id')
        ->select(DB::raw('num_orden, num_inscripcion, titular, num_doc, sorteos.nombre as sorteo, 
        ganador, aspirantes.id as id, categorias.nombre as categoria'))
        ->where('activo', '1')
        ->orderBy('ganador' , 'desc' , 'num_orden', 'asc', 'created_at', 'desc')
        ->simplePaginate(3000);

        return $lista;
    }

    public function ganadores_ajax() {
        $lista = DB::table('aspirantes')->distinct()
        ->join('sorteos',  'aspirantes.sorteo_id', '=', 'sorteos.id')
        ->join('categorias', 'categorias.id', 'aspirantes.categoria_id')
        ->select(DB::raw('num_orden, num_inscripcion, titular, num_doc, sorteos.nombre as sorteo, 
        ganador, aspirantes.id as id, categorias.nombre as categoria'))
        ->where('activo', '1')
        ->where('ganador', '1')
        ->orderBy('ganador' , 'desc' , 'num_orden', 'asc', 'created_at', 'desc')
        ->simplePaginate(3000);

        return $lista;
    }

    public function excluidos_ajax(Request $request) {
        $lista = DB::table('aspirantes')->distinct()
        ->join('sorteos',  'aspirantes.sorteo_id', '=', 'sorteos.id')
        ->join('categorias', 'categorias.id', 'aspirantes.categoria_id')
        ->select(DB::raw('titular'))
        ->where('activo', '1')
        ->where('ganador', '1')
        ->orderBy('ganador' , 'desc' , 'num_orden', 'asc', 'created_at', 'desc')
        ->get();

        $arrayExcluidos = [];
        foreach ($lista as $l) {
            array_push($arrayExcluidos, $l->titular);
        }
        
        $lista = DB::table('aspirantes')->distinct()
        ->join('sorteos',  'aspirantes.sorteo_id', '=', 'sorteos.id')
        ->join('categorias', 'categorias.id', 'aspirantes.categoria_id')
        ->select(DB::raw('num_orden, titular, categorias.nombre as categoria'))
        ->where('activo', '1')
        ->where('ganador', '0')
        ->where('sorteos.nombre', $request->get('sorteo'))
        ->where('categorias.nombre', $request->get('categoria'))
        ->whereIn('titular', $arrayExcluidos)
        ->groupBy('num_orden', 'titular', 'categorias.nombre')
        ->orderBy('ganador' , 'desc' , 'num_orden', 'asc', 'created_at', 'desc')
        ->simplePaginate(3000);

        return $lista;
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo = 'Lista de aspirantes por sorteos';

        $categorias = DB::table('categorias')->orderBy('nombre')->get();
        $sorteos = DB::table('sorteos')->where('activo', '1')->orderBy('nombre')->get();

        return view($this->path.'.create', compact('categorias', 'sorteos'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $safyc = new Safyc();
        $archivo_legal->expediente = $expediente;

        $archivo_legal->save();

        \Session::flash('flash_message', 'El archivo ' . $archivo->resolucion . ' fue subido correctamente ' );
        
       return view($this->path. '.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $entidad= \App\Aspirante::find($id);

        $aspirante = \App\Aspirante::where('num_doc', '=', $entidad->num_doc)
        ->where('ganador', '1')
        ->get();

        if(!count($aspirante)){
            $entidad->ganador = 1;
            $entidad->save();
            return $id;
        } else
        {
            return 'existente';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
