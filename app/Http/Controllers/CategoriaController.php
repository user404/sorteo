<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CategoriaController extends Controller
{
    private $path = 'categoria';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo = 'Lista de aspirantes por sorteos';
        return view($this->path.'.index', compact('titulo'));
        return view($this->path. '.index');


    }

    public function categorias_ajax(){

        $lista = DB::table('categorias')->distinct()
        ->select(DB::raw('id, nombre'))
        ->groupBy('categorias.id')
        ->orderBy('id' , 'desc', 'created_at', 'desc')
        ->simplePaginate(3000);

        return $lista;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulo = 'Lista de aspirantes por sorteos';

//        $categorias = DB::table('categorias')->orderBy('nombre')->get();
  //      $sorteos = DB::table('sorteos')->where('activo', '1')->orderBy('nombre')->get();

//        return view($this->path.'.create', compact('categorias', 'sorteos'));
        return view($this->path.'.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entidad = new Categoria();
        $entidad->nombre = $request->nombreCategoria;
        $entidad->save();

        $lista = DB::table('categorias')->distinct()->get();
        return $lista;   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

