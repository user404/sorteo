<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Collection;
use App\Errores;
use Carbon\Carbon;
use Response;
use App\Sorteo;

class SorteoController extends Controller
{

    private $path = 'sorteo';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo = 'Lista de aspirantes por sorteos';
        return view($this->path.'.index', compact('titulo'));
        return view($this->path. '.index');


    }

    public function sorteos_ajax(){

        $lista = DB::table('aspirantes')->distinct()
        ->join('sorteos',  'aspirantes.sorteo_id', '=', 'sorteos.id')
        ->select(DB::raw(' sorteos.id as id, sorteos.nombre, count(aspirantes.id) aspirantes, activo'))
        ->groupBy('sorteos.id')
        ->orderBy('ganador' , 'desc' , 'num_orden', 'asc', 'created_at', 'desc')
        ->simplePaginate(3000);

        return $lista;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entidad = new Sorteo();
        $entidad->nombre = $request->nombre;
        $entidad->activo = 1;
//        return $entidad;
        $entidad->save();

        $lista = DB::table('sorteos')->distinct()->get();
        return $lista;
        //      $ultimo = $user = DB::table('sorteos')->orderBy('id', 'desc')->limit(1)->get();        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $entidad= \App\Sorteo::find($id);     
        if($entidad->activo == 1){
            $entidad->activo = 0;
        } else {
            $entidad->activo = 1;
        }

        $entidad->save();

        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
