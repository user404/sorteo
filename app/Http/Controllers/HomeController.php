<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->action('AspiranteController@index');
//        return redirect()->action('DeudaController@index', ['id' => 0]);
//        view('/listar_datos/0'); //($this->path.'.dashboard');
    }
}
